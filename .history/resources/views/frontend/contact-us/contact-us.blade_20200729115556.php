@extends('frontend.layout.master') @section('title', 'Welcome to SDF') @section('active-contact-us', 'active') @section('content') @section ('appbottomjs')
<script src='https://www.google.com/recaptcha/api.js'></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css">

<script type="text/javascript">
    $(document).ready(function() {
        $("#submit-contact").submit(function(event) {
            submit(event);
        })

        @if(Session::has('invalidData'))
        $("#form").show();
        @endif

    })
    @if(Session::has('msg'))
    toastr.success("{{ Session::get('msg') }}");
    @endif

    function submit(event) {

        name = $("#name").val();
        company = $("#company").val();
        email = $("#email").val();
        message = $("#message").val();
        recaptcha = $('#g-recaptcha-response').val();

        if (name != 0) {
            if (validateEmail(email)) {
                if (company) {

                    if (recaptcha != "") {

                        @if(Session::has('msg'))
                        toastr.error("Please check robot verification");
                        @endif
                    } else {
                        toastr.error("Please check robot verification");
                        event.preventDefault();
                        $("#g-recaptcha-response").focus();
                    }
                } else {
                    toastr.error("Please Select company");
                    event.preventDefault();
                    $("#company").focus();
                }
            } else {
                toastr.error("Please give us correct email");
                event.preventDefault();
                $("#email").focus();
            }
        }
    } else {
        toastr.error("Please enter your name");
        event.preventDefault();
        $("#name").focus();
    }

    function showApplicationForm() {
        form = $("#form");
        if (form.is(":visible")) {
            form.hide();
        } else {
            form.show();
        }
    }

    function validateEmail(email) {
        var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(email);
    }

    function validatePhone(phone) {
        return phone.match(/(^[00-9].{8}$)|(^[00-9].{9}$)/);
    }
</script>
@endsection @if($banner)
<div class="page-header" style="background-image: url({{ asset ($banner->image)}});">
    <div class="container">
        <div class="row">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="page-breadcrumb">
                    <ol class="breadcrumb">
                        <li><a href="#">{{__('general.contact-us')}}</a></li>
                        <li class="active">{{__('general.contact-us')}}</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
</div>
@endif

<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="wrapper-content bg-white ">
                <div class="about-section pinside40">
                    <div class="row">
                        <div class="col-xl-12 director-title-top mb-4">
                            <div class="">
                                <div class="title">
                                    <h3 class="">{{__('general.contact-us')}}</h3>
                                </div>
                                <div class="social">
                                    <a href="https://www.facebook.com/sdfcambodia/">
                                        <i class="fa fa-facebook" style=" color: #006cb9;"></i>
                                    </a>
                                    <a<i class="fa fa-print" style="margin-left: 10px; color: #006cb9;"></i>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-5 col-lg-5 col-md-5 col-sm-12 col-12">

                            <div class="mb60">

                                <ul class="list-group list-group-flush">
                                    <li class="list-group-item">
                                        <div class="absolutetop">
                                            <a href="https://www.google.com/maps/place/Ministry+of+Economy+and+Finance+of+Cambodia/@11.576679,104.9183918,17z/data=!3m1!4b1!4m5!3m4!1s0x310951434d493e03:0xb1a605e9a569ec8b!8m2!3d11.576679!4d104.9205805"><i class="fa fa-map-marker" style="font-size: 15px;width: 25px;"></i></a>
                                        </div>
                                        <div style="margin-left:30px;">
                                            <a href="https://www.google.com/maps/place/Ministry+of+Economy+and+Finance+of+Cambodia/@11.576679,104.9183918,17z/data=!3m1!4b1!4m5!3m4!1s0x310951434d493e03:0xb1a605e9a569ec8b!8m2!3d11.576679!4d104.9205805"> {{__('general.email-address-contact')}}</a>
                                        </div>
                                    </li>
                                    <li class="list-group-item">
                                        <div class="absolutetop">
                                            <a href="#"><i class="fa fa-phone" style="font-size: 15px;width: 25px;"></i></a>
                                        </div>
                                        <div style="margin-left:30px;">
                                            <a href="#"> {{__('general.phone-number2')}} </a>
                                        </div>
                                    </li>
                                    <li class="list-group-item">
                                        <div class="absolutetop">
                                            <a href="https://www.google.com/maps/place/Ministry+of+Economy+and+Finance+of+Cambodia/@11.576679,104.9183918,17z/data=!3m1!4b1!4m5!3m4!1s0x310951434d493e03:0xb1a605e9a569ec8b!8m2!3d11.576679!4d104.9205805"><i class="fa fa-envelope" style="font-size: 14px;width: 25px;"></i></a>
                                        </div>
                                        <div style="margin-left:30px;">
                                            <a href=""> {{__('general.email-contact1')}} </a>
                                        </div>
                                    </li>
                                    <li class="list-group-item">
                                        <div class="absolutetop">
                                            <a href=""><i class="fa fa-globe" style="font-size: 15px;width: 25px;"></i></a>
                                        </div>
                                        <div style="margin-left:30px;">
                                            <a href="http://demo.sdfcambodia.org/"> www.sdfcambodia.org</a>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>

                        <div class="col-xl-7 col-lg-7 col-md-7 col-sm-12 col-12">
                            {{-- <div class="map" id="googleMap"></div> --}}
                        </div>
                        <div class="col-xs-12 mt-3">
                            <h4 class="mb30" style=""> {{__('general.sent-us-message')}}
                            </h4>
                        </div>

                        <div class="col-12">
                            @if(Session::has('msg'))
                            <div class="alert alert-success" style="text-align: center;" role="alert">
                                {{ Session::get('msg') }}
                            </div>
                            @endif @if (count($errors) > 0)
                            <div class="alert alert-danger" role="alert">
                                Sending fail! Please Try again!
                            </div>
                            @endif @if (count($errors) > 0) @foreach ($errors->all() as $error)
                            <div class="alert alert-danger" role="alert">
                                {{ $error }}
                            </div>
                            @endforeach @endif
                        </div>
                        <div class="col-xs-12">
                            <form class="contact-us" method="post" action="{{ route('sendmessage') }}">
                                {{ csrf_field() }} {{ method_field('PUT') }}
                                    <!-- Text input-->
                                <div class="row">
                                    <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                                        <div class="form-group">
                                            <label class="sr-only control-label" for="name">{{__('general.name')}}<span class=" "> </span></label>
                                            <input id="name" name="name" type="text" placeholder="{{__('general.name')}}" class="form-control input-md" required>
                                        </div>
                                    </div>
                                    <!-- Text input-->
                                    <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                                        <div class="form-group">
                                            <label class="sr-only control-label" for="email">{{__('general.email')}}<span class=" "> </span></label>
                                            <input id="email" name="email" type="email" placeholder="{{__('general.email')}}" class="form-control input-md" required>
                                        </div>
                                    </div>

                                    <!-- Text input-->
                                    <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                                        <div class="form-group">
                                            <label class="sr-only control-label" for="email">{{__('general.company')}}<span class=" "> </span></label>
                                            <input id="company" name="company" type="company" placeholder="{{__('general.company')}}" class="form-control input-md" required>
                                        </div>
                                    </div>
                                    <!-- Text input-->
                                    <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                                        <div class="form-group">
                                            <label class="sr-only control-label" for="phone">{{__('general.phone')}}<span class=" "> </span></label>
                                            <input id="phone" name="phone" type="text" placeholder="{{__('general.phone')}}" class="form-control input-md" required>
                                        </div>
                                    </div>
                                    <!-- Select Basic -->
                                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                        <div class="form-group">
                                            <label class="control-label" for="message"> </label>
                                            <textarea class="form-control" name="message" id="message" rows="7" name="message" placeholder="{{__('general.message')}} *"></textarea>
                                        </div>
                                    </div>
                                    <!-- Button -->
                                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                        <button type="submit" class="btn btn-default" placeholder="Submit">{{__('general.submit')}}</button>
                                    </div>
                                </div>
                            </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    function initMap() {
        var myLatLng = {
            lat: 11.576643,
            lng: 104.920587
        };

        var map = new google.maps.Map(document.getElementById('googleMap'), {
            zoom: 15,
            center: myLatLng,
            scrollwheel: false,

        });
        var image = "{{ asset ('public/frontend/images/pinmap.png') }}";
        var marker = new google.maps.Marker({
            position: myLatLng,
            map: map,
            icon: image,
            title: 'SDF'

        });
    }
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCRQUxXDUoOYY5VGCKnOA6MuCIeuhzSn84&amp;callback=initMap" async defer></script>
<style>
    .absolutetop {
        position: absolute;
        z-index: 99;
    }
</style>
@endsection