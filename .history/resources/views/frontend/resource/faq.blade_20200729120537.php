@extends('frontend.layout.master') @section('title', 'Welcome to SDF') @section('active-resources', 'active') @section('content')

   @if($banner)
   <div class="page-header" style="background-image: url({{ asset ($banner->image)}});">
    <div class="container">
        <div class="row">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="page-breadcrumb">
                    <ol class="breadcrumb">
                        <li><a href=" {{ route('selection', $locale)}} ">{{__('general.resources')}}</a></li>
                        <li class="active">FAQ</li>
                    </ol>
                </div>
            </div>

        </div>
    </div>
</div>
 @endif

<div class="wrapper-content bg-white pinside40 container">

    <div class="row">
        <div class="col-xl-12 director-title-top mb-4">
           
                <div class="title">
                    <h3 class="">{{__('general.faqs')}}</h3>
                </div>
                <div class="social">
                    <a target="_blank"href="https://www.facebook.com/sdfcambodia/">
                        <i class="fa fa-facebook" style=" color: #006cb9;"></i>
                    </a>
                    <a onclick=""><i class="fa fa-print" style="margin-left: 10px; color: #006cb9;"></i></a>
                </div>
            
        </div>
    </div>

   
@foreach( $faqs as $row)
    <div class="section">
        <div class="row">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="section-title mb">
                <h4>{{$row->title}}</h4>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 st-accordion">
                <div class="panel-group" id="accordion-1" role="tablist" aria-multiselectable="true">
                    
                    @php($questions = $row->questions)
                    @foreach($questions as $question)
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingFive">
                            <h1 class="panel-title"> <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse-{{ $question->id }}" aria-expanded="true" aria-controls="collapse-{{ $question->id }}"><i class="fa fa-plus-circle sign"></i>{{ $question->question }}</a> </h1>
                        </div>
                        <div id="collapse-{{ $question->id }}" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFive">
                            <div class="panel-body">{{ $question->answer }}</div>
                            </div>
                        </div>
                    @endforeach

                  
                </div>
            </div>
        </div>
    </div>
@endforeach
   
   
</div>
@endsection