@extends('frontend.layout.master') @section('title', 'Welcome to SDF') @section('active-resources', 'active') @section('content') @if($banner)
<div class="page-header" style="background-image: url({{ asset ($banner->image)}});">
    <div class="container">
        <div class="row">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="page-breadcrumb">
                    <ol class="breadcrumb">
                        <li><a href=" {{ route('guideline-proposal', $locale) }} ">{{__('general.resources')}}</a></li>
                        <li class="active">{{__('general.guidelines-for-proposal-submission')}}</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
</div>
@endif
<div class=" ">
    <!-- content start -->
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="wrapper-content bg-white ">
                    <div class="about-section pinside40">
                        <div class="row">

                            <div class="col-xl-12 director-title-top mb-4">
                                <div class="">
                                    <div class="title">
                                        <h2 class="">{{__('general.guidelines-for-proposal-submission')}}</h2>
                                    </div>
                                    <div class="social">
                                        <a target="_blank"href="https://www.facebook.com/sdfcambodia/">
                                            <i class="fa fa-facebook" style=" color: #006cb9;"></i>
                                        </a>
                                        <a on><i class="fa fa-print" style="margin-left: 10px; color: #006cb9;"></i></a>
                                    </div>
                                </div>
                            </div>

                            <div class="">
                                <div class="title">
                                    <h2 class="">{{__('general.Proposal Submission Procedures')}}</h2>
                                </div>
                                <p class="">{{__('general.5step-content')}}</p>
                                <h3 class="">{{__('general.ilustration-of-proposal-submission')}}</h3>
                                <div class="row">
                                    <div class="col-xl-12 text-center">
                                        <img src="{{ asset ('public\frontend\images\test.png') }}" alt="Image 5 step" class="img-responsive">
                                    </div>
                                </div>
                                <h3 class="">{{__('general.acceptance-and-deadline')}}</h3>
                                <p  style=";">{{__('general.acceptance-and-deadline-content')}}</p>
                                
                                <h3 class="" style="solid #006cb9;">{{__('general.introduction-about-the-sdf-pilot-project')}}</h3>
                                <p  class=""><span style="color:#006cb9; font-size:18px;">{{__('general.about-sdf-v2')}}</span>{{__('general.introduction-about-the-sdf-pilot-project-content')}}</p>
                                <p>{{__('general.about-sdf-v3')}}</p>
                                <h3 class="" style=" solid #006cb9;">{{__('general.priorities-and-target-group')}}</h3>
                                <p  style="">{{__('general.priorities-and-target-group-content')}}</p>
                                <h3 class="" style="">{{__('general.who-can-apply')}}</h3>
                                <p  style="">{{__('general.who-can-apply-content1')}}</p>
                                <p  style="">{{__('general.who-can-apply-content2')}}</p>
                                <p  style=""><i>{{__('general.who-can-apply-content3')}}</i></p>

                                <h3 class="" style="">{{__('general.type-of-skills-training-and-possible')}}</h3>
                                <p  style="">{{__('general.type-of-skills-training-and-possible-content')}}</p>
                                <h3 class="" style="">{{__('general.unit-cost-of-the-training')}}</h4>
                                        <p style=";">{{__('general.unit-cost-of-the-training-content')}}</p>
                                        <h3 class="" style="">{{__('general.eligible-expenses-and-ineligible-expenses')}}</h3>
                                <p  style="">{{__('general.eligible-expenses-and-ineligible-expenses-content')}}</p>
                                <h3 class="" style="">{{__('general.language-of-the-proposal')}}</h3>
                                <p  style="">{{__('general.language-of-the-proposal-content')}}</p>
                                <h3 class="" style="">{{__('general.reporting-requirements')}}</h3>
                                <p  style="">{{__('general.reporting-requirements-content1')}}</p>
                                <p  style="">{{__('general.reporting-requirements-content2')}}</p>
                            </div>
                            <table class="table">
                                <tbody>
                                    <tr>
                                        <td></td>
                                        <td><b>{{__('general.KPI')}}</b></td>
                                        <td><b>{{__('general.Large-Firms')}}</b></td>
                                        <td><b>{{__('general.SMEs')}}</b></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <th>{{__('general.a')}}</th>
                                        <td>{{__('general.Drop-out-rate')}} </td>
                                        <td>{{__('general.≤ 25%-of-enrolled-students')}}</td>
                                        <td>{{__('general.≤ 25%-of-enrolled-students')}}</td>
                                    </tr>
                                    <tr>
                                        <th>{{__('general.b')}}</th>
                                        <td>{{__('general.Employment-rate-(for Pre-employment only)')}}</td>
                                        <td>{{__('general.≥ 75%-of-graduated-students-are-formally-employed-in-any-companies-within-3-months-after-training-completion-in-relevant-occupation')}} </td>
                                        <td>{{__('general.≥ 75%-of-graduated-students-are-formally-employed-in-any-companies-within-3-months-after-training-completion-in-relevant-occupation')}}</td>
                                    </tr>
                                    <tr>
                                        <th>{{__('general.c')}}</th>
                                        <td>{{__('general.Salary-of-trainees-after-3-months-of-completion')}}
                                            <br />
                                        {{__('general.up-skilling')}}
                                        <br />
                                        {{__('general.pre-employment1')}}
                                        </td>
                                        <td><br><br>  {{__('general.≥ 10%-of-current-salary')}}
                                            <br />
                                            {{__('general.up-skilling1')}}
                                        </td>
                                        <td><br><br>{{__('general.≥ 10%-of-current-salary')}}
                                            <br />
                                            {{__('general.≥ 10%-of-current-salary1')}}
                                        </td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    {{-- <tr>
                                        <td>{{__('general.pre-employment')}}</td>
                                        <td>{{__('general.≥ 110% of minimum wage of garment and footwear sector (only basic salary)')}}</td>
                                        <td>{{__('general.≥110% of the reference salary $150 (only basic salary)')}}</td>
                                        <td></td>
                                    </tr> --}}
                                    {{-- <tr>
                                        <td>{{__('general.Drop-out rate in ToT ≤ 10% of enrolled trainers')}}</td>
                                        <td>{{__('general.Number of trainers completed ToT successfully ≥ 90% of enrolled trainers')}}</td>
                                        <td>{{__('general.Number of trainees that successful trainer in ToT provided training within 5 months after ToT completion ≥ 25 trainees')}}</td>
                                        <td></td>
                                    </tr> --}}
                                </tbody>
                            </table>
                            <div class="row">
                                <div class="col-12">
                                    <h3 class="" style="">{{__('general.disbursement-plan')}}</h3>
                                </div>
                                <div class="col-12">
                                    <p>{{__('general.disbursement-plan-content1')}}</p>
                                    <p>{{__('general.disbursement-plan-content2')}}</p>
                                    <p>{{__('general.disbursement-plan-content3')}}</p>
                                    <p>{{__('general.disbursement-plan-content4')}}</p>
                                    <p><span><b>{{__('general.note')}}</b>  {{__('general.note-content')}}</span></p>
                                    <br />
                                </div>

                                <div class="col-12">
                                    <h3 class="" style="">{{__('general.recipients-of-financing')}}</h3>
                                    <p>{{__('general.recipients-of-financing-content')}}</p>
                                    <br />
                                </div>
                                <div class="col-12">
                                    <h3 class="" style="">{{__('general.contact-of-further-inquiries')}}</h3>
                                </div>
                                
                                <div class="col-12">
                                    <p>{{__('general.contact-of-further-inquiries-content')}}</p>
                                </div>

                            </div>
                            <div class="col-xl-12">
                                <p>{{__('general.contact-of-further-inquiries-phone')}}</p>
                                <p>{{__('general.contact-of-further-inquiries-telegram')}}</p>
                                <p>{{__('general.email-contact2')}}</p>
                                <br />
                            </div>
                        </div>

                        <div>
                            <h4> {{__('general.download-the-whole-page')}} <a href="http://www.sdfcambodia.org/public/uploads/file/guideline-for-proposal-submission.pdf" style="color: #006cb9; "><abbr title="Click On PDF for download">PDF</abbr></a></h4>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection