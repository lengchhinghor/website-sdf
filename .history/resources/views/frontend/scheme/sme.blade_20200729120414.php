 @extends('frontend.layout.master')
 @section('title', 'Welcome to SDF')
 @section('active-scheme', 'active')
 @section('content')

    @if($banner)
    <div class="page-header" style="background-image: url({{ asset ($banner->image)}});">
        <div class="container">
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <div class="page-breadcrumb">
                        <ol class="breadcrumb">
                        <li><a href="{{ route('sme', $locale) }}">{{__('general.scheme')}}</a></li>
                        <li class="">{{__('general.training-for-smes')}}</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endif
    <div class=" ">
        <!-- content start -->
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="wrapper-content bg-white ">
                        <div class="about-section pinside40">
                            <div class="row">
                                <div class="col-xl-12 director-title-top mb-4">
                                <div class="">
                                    <div class="title">
                                        <h3 class="">{{__('general.training-for-smes')}}</h3>
                                    </div>

                                    <div class="social">
                                        <a target="_blank"href="https://www.facebook.com/sdfcambodia/">
                                            <i class="fa fa-facebook" style=" color: #006cb9;"></i>
                                        </a>
                                        <a onclick=""><i class="fa fa-print" style="margin-left: 10px; color: #006cb9;"></i></a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12">
                                    {!!$data->content!!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </div>
 @endsection
