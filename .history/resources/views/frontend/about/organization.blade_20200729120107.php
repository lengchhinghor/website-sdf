@extends('frontend.layout.master')
@section('title', 'Welcome to SDF')
@section('active-about-us', 'active')
@section('content')

@if($banner)
 <div class="page-header" style="background-image: url({{ asset ($banner->image)}});">
        <div class="container">
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <div class="page-breadcrumb">
                        <ol class="breadcrumb">
                        <li><a href=" {{ route('organization', $locale) }} ">{{__('general.about-us')}}</a></li>
                        <li class="active">{{__('general.sdf-organizational-structure')}}</li>
                        </ol>
                    </div>
                </div>

                 <div class="container ">
                        <div class="row">
                            <div class="col-xl-11 col-lg-6 col-md-6 col-sm-12 col-12">
                            <div><img src="{{ asset ('public/frontend/camcyber/stucture.png') }}" alt=""></div>
                        </div>
                    </div>
                 </div>
            </div>
        </div>
    </div>
    @endif
 
    <div class="wrapper-content bg-white about-section pinside40 container">
        
        <div class="row">
            <div class="col-xl-12 director-title-top mb-4">
                <div class="">
                    <div class="title">
                        <h3 class="">{{__('general.sdf-organizational-structure')}}</h3>
                    </div>
                    <div class="social">
                        <a target="_blank"href="https://www.facebook.com/sdfcambodia/">
                            <i class="fa fa-facebook" style=" color: #006cb9;"></i>
                        </a>
                        <a></a><i class="fa fa-print" style="margin-left: 10px; color: #006cb9;"></i>
                    </div>
                </div>
            </div>
            <div class="col-xl-12 text-center">
            @foreach($organization as $row)
            <div ><img src="{{ asset ($row->image) }}" class="img-fluid" alt="Responsive image"></div>
            @endforeach
            </div>
        </div>
        
    </div>
                
    @endsection