 @extends('frontend.layout.master')
 @section('title', 'Welcome to SDF')
 @section('active-about-us', 'active')
 @section('content')

 @if($banner)
 <div class="page-header" style="background-image: url({{ asset ($banner->image)}});">
        <div class="container">
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <div class="page-breadcrumb">
                        <ol class="breadcrumb">
                        <li><a href=" {{ route('history', $locale) }} ">{{__('general.about-us')}}</a></li>
                        <li class="active">{{__('general.history-of-project')}}</li>
                        </ol>
                    </div>
                </div>
              
            </div>
        </div>
    </div>
  @endif

    <div class="wrapper-content bg-white about-section pinside40 container">
        
            <div class="row">
            
                <div class="col-12 director-title-top mb-4 page-title-cnt">
                    
                    <div class="title">
                        <h3 >{{__('general.history-of-project')}}</h3>
                    </div>

                    <div class="social">
                        <a target="_blank"href="https://www.facebook.com/sdfcambodia/">
                            <i class="fa fa-facebook" style=" color: #006cb9;"></i>
                        </a>
                        <i class="fa fa-print" style="margin-left: 10px; color: #006cb9;"></i>
                    </div>
                </div>
                        
                <div class="col-12">
                    {!! $data->content_part1 !!}
                </div>
            </div>
        
    </div>
                
        
 @endsection
