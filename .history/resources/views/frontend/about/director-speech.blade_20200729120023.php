 @extends('frontend.layout.master') 
 @section('title', 'Welcome to SDF')
 @section('active-about-us', 'active')
 @section('content')

@if($banner)
 <div class="page-header" style="background-image: url({{ asset ($banner->image)}});">
    <div class="container">
        <div class="row">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="page-breadcrumb">
                    <ol class="breadcrumb">
                    <li><a href="{{ route('director-speech',$locale)}} ">{{__('general.about-us')}}</a></li>
                        <li class="active">{{__('general.director-speech')}}</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
</div>
@endif

<div class="wrapper-content bg-white about-section pinside40 container">
    
        <div class="row">
            <div class="col-xl-12 director-title-top mb-4">
                <div class="">
                    <div class="title">
                        <h3 class="">{{__('general.director-speech')}}</h3>
                    </div>
                    <div class="social">
                        <a target="_blank"href="https://www.facebook.com/sdfcambodia/">
                            <i class="fa fa-facebook" style=" color: #006cb9;"></i>
                        </a>
                        <a onclick="window.print"><i class="fa fa-print" style="margin-left: 10px; color: #006cb9;"></i></a>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xl-6">
                {{-- <img src="http://www.sdfcambodia.org/wp-content/uploads/2019/01/Cheang-Vannarith-300x238.jpg" class="img-fluid"/> --}}
                <img src=" {{ asset ('public/frontend/camcyber/director.jpg') }}" class="img-fluid"/>
            </div>  
            <div class="col-xl-6">
                {!! $data->content_part1 !!}
            </div>           
        </div>
        <div class="row mt-3">
            <div class="col-xl">
                {!! $data->content_part2 !!}
            </div>            
        </div>
    
</div>
            
@endsection