
<?php

Route::get('/', 				['as' => 'index', 			'uses' => 'TrainingSMSController@index']);
Route::get('/create', 			['as' => 'create', 			'uses' => 'TrainingSMSController@create']);
Route::put('/', 				['as' => 'store', 			'uses' => 'TrainingSMSController@store']);
Route::get('/{id}', 			['as' => 'edit', 			'uses' => 'TrainingSMSController@showEditForm']);
Route::post('/', 				['as' => 'update', 			'uses' => 'TrainingSMSController@update']);

Route::delete('/{id}', 			['as' => 'trash', 			'uses' => 'TrainingSMSController@trash']);
Route::post('update-status', 	['as' => 'update-status', 	'uses' => 'TrainingSMSController@updateStatus']);
