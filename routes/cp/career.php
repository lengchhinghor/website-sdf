<?php
//:::::::::::::>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Slide

Route::group([], function () {
	Route::get('/', 				['as' => 'index', 			'uses' => 'CareerController@index']);
	Route::get('/create', 			['as' => 'create', 			'uses' => 'CareerController@create']);
	Route::get('/{id}', 			['as' => 'edit', 			'uses' => 'CareerController@edit']);
	Route::post('/', 				['as' => 'update', 			'uses' => 'CareerController@update']);
	
	Route::put('/', 				['as' => 'store', 			'uses' => 'CareerController@store']);
	Route::delete('/{id}', 			['as' => 'trash', 			'uses' => 'CareerController@trash']);
	Route::post('order', 			['as' => 'order', 			'uses' => 'CareerController@order']);
	Route::post('status', 			['as' => 'update-status', 	'uses' => 'CareerController@updateStatus']);


	Route::get('/{id}/apply', 			['as' => 'apply', 			'uses' => 'CareerapplyController@index']);

	Route::get('/{id}/edit-apply/{apply_id}', 			['as' => 'edit-apply', 			'uses' => 'CareerapplyController@edit']);
	Route::post('/{id}/update-apply/{apply_id}', 				['as' => 'update-apply', 			'uses' => 'CareerapplyController@update']);

	Route::delete('/{id}/trash-apply/{apply_id}', 			['as' => 'trash-apply', 			'uses' => 'CareerapplyController@trash']);
	Route::post('/{id}/order-apply', 			['as' => 'order-apply', 			'uses' => 'CareerapplyController@order']);
	Route::post('{id}/status-apply', 			['as' => 'update-status-apply', 	'uses' => 'CareerapplyController@updateStatus']);
});	