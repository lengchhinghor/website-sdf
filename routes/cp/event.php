<?php
//:::::::::::::>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Event

Route::group([], function () {
	Route::get('/', 				['as' => 'index', 				'uses' => 'EventController@index']);
	Route::get('/feature', 			['as' => 'feature', 			'uses' => 'EventController@feature']);
	Route::get('/creates', 			['as' => 'create', 				'uses' => 'EventController@create']);
	Route::put('/', 				['as' => 'store', 				'uses' => 'EventController@store']);
	Route::get('/{id}', 			['as' => 'edit', 				'uses' => 'EventController@edit']);
	Route::post('/', 				['as' => 'update', 				'uses' => 'EventController@update']);
	
	Route::delete('/{id}', 			['as' => 'trash', 				'uses' => 'EventController@trash']);
	Route::post('status', 			['as' => 'update-status', 		'uses' => 'EventController@updateStatus']);
	Route::post('/order', 			['as' => 'order', 				'uses' => 'EventController@order']);
	Route::post('update-featured', 	['as' => 'update-featured', 	'uses' => 'EventController@updateFeatured']);
	
});	