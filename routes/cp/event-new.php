<?php
Route::get('/', 				['as' => 'index', 			'uses' => 'EventNewController@index']);
Route::get('/create', 			['as' => 'create', 			'uses' => 'EventNewController@create']);
Route::put('/', 				['as' => 'store', 			'uses' => 'EventNewController@store']);
Route::get('/{id}', 			['as' => 'edit', 			'uses' => 'EventNewController@showEditForm']);
Route::post('/', 				['as' => 'update', 			'uses' => 'EventNewController@update']);

Route::delete('/{id}', 			['as' => 'trash', 			'uses' => 'EventNewController@trash']);
Route::post('update-status', 	['as' => 'update-status', 	'uses' => 'EventNewController@updateStatus']);
