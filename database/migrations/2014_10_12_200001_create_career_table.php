<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCareerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('career', function (Blueprint $table) {
            $table->increments('id', 11);
            
            $table->integer('data_order')->nullable();
			$table->string('en_title', 500)->nullable();
            $table->string('kh_title', 500)->nullable();
            $table->text('en_description')->nullable();
            $table->text('date')->nullable();
            $table->date('kh_description')->nullable();
            $table->text('en_location')->nullable();
            $table->text('kh_location')->nullable();
            $table->text('dateline')->nullable();
            $table->string('feature_image', 250)->nullable();
            $table->boolean('is_published')->default(0);
            $table->boolean('is_featured')->default(0);
            //The field that will appear for almost tables
            $table->integer('creator_id')->unsigned()->index()->nullable();
            $table->integer('updater_id')->unsigned()->index()->nullable();
            $table->integer('deleter_id')->unsigned()->index()->nullable();
            $table->timestamps();
            $table->softDeletes();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('career');
    }
}
