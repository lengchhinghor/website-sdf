@extends('frontend.layout.master')
@section('title', 'Welcome to SDF')
@section('active-news', 'active')
@section('content')

@if($banner)
<div class="page-header" style="background-image: url({{ asset ($banner->image)}});">
       <div class="container">
           <div class="row">
               <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                   <div class="page-breadcrumb">
                       <ol class="breadcrumb">
                            <li><a href="#" style="color:white;">{{__('general.news-and-events')}}</a></li>
                           <li class="active">{{__('general.news')}}</li>
                       </ol>
                   </div>
               </div>
           </div>
       </div>
   </div>
 @endif
 
    <div class="wrapper-content container bg-white about-section pinside40">
        
        {{-- ============================== Page Header ================================ --}}
        <div class="row">
            <div class="col-xl-12 director-title-top mb-4">
                <div class="title">
                    <h3 class="">{{__('general.news')}}</h3>
                </div>
            </div>
        </div>
        
            {{-- ============================== News Items ================================ --}}
        <div class="row ">
            
            <div class="container-fluid" >
                @foreach ($data as $row)
                    <div class="row news-items ">   
                        <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12 image-cnt">
                            <a href="{{ route('news-detail',  ['locale' => $locale, 'id'=>$row->id]) }}"  >
                                <img src="{{ asset ($row->image) }}" alt="" class="img-fluid">
                            </a>
                        </div>
                        <div class="col-xl-8 col-lg-8 col-md-8 col-sm-12 col-12">
                            
                            <h2 >
                                <a href="{{ route('news-detail',  ['locale' => $locale, 'id'=>$row->id]) }}" class="news-title" style="color:black;" > {{ $row->title }}  <span class="meta-date">{{$row->date}}</span> 
                                </a>
                            </h2>
                            <p class="news-description"> {{$row->description}}
                                <a href="{{ route('news-detail',  ['locale' => $locale, 'id'=>$row->id]) }}"​ class="continue-reading" >{{__('general.read-more')}}</a>
                            </p>
                        </div>
                    </div>
                @endforeach
            </div>

        </div>

            {{-- ============================== Pagination ================================ --}}
        <div class="row">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 text-center ">
                <br />
                <div class="st-pagination d-flex justify-content-center">
                    <ul class="pagination">
                    {{ $data->links('vendor.pagination.frontend-html') }}
                    </ul>
                </div>
            </div>
        </div>
        
    </div>
    
    
@endsection
