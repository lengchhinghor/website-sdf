<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" type="image/x-icon" href="{{ asset('public/frontend/images/favicon/faviconsdf.png') }}">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="Skill Development Fund (SDF), Ministry of Economic and Finance">
    <meta name="keywords" content="Skill Development Fund (SDF), Ministry of Economic and Finance">
    <title>Skills Development Fund - SDF</title>
    <!-- Bootstrap -->
    <link href="{{ asset ('public/frontend/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset ('public/frontend/css/style.css') }}" rel="stylesheet">
    <link href="{{ asset ('public/frontend/css/font-awesome.min.css') }}" rel="stylesheet">
    <link href="{{ asset ('public/frontend/css/fontello.css') }}" rel="stylesheet">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700%7CMerriweather:300,300i,400,400i,700,700i" rel="stylesheet">
    <!-- owl Carousel Css -->
    <link href="{{ asset ('public/frontend/css/owl.carousel.css') }}" rel="stylesheet">
    <link href="{{ asset ('public/frontend/css/owl.theme.css') }}" rel="stylesheet"> 

    <!-- HTML5 shim and Respond.js') }} for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js') }} doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js') }}"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js') }}"></script>
    <![endif]-->

    <link href="https://fonts.googleapis.com/css?family=Hanuman" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Moul" rel="stylesheet">

    <!--Start of Tawk.to Script-->
    @if($locale=="kh")
       <!--  <link rel="stylesheet" href="{{ asset('public/frontend/kh-language.css') }}">  -->
    @endif    
    <script type="text/javascript">
        var Tawk_API = Tawk_API || {},
            Tawk_LoadStart = new Date();
        (function() {
            var s1 = document.createElement("script"),
                s0 = document.getElementsByTagName("script")[0];
            s1.async = true;
            s1.src = 'https://embed.tawk.to/5d51875b77aa790be32e8dd5/default';
            s1.charset = 'UTF-8';
            s1.setAttribute('crossorigin', '*');
            s0.parentNode.insertBefore(s1, s0);
        })();
    </script>
    <!--End of Tawk.to Script-->
</head>

<body>

    <div class="header-standard ">
       
        <div class="top-header">
            <div class="container d-none d-sm-block ">
                <div class="row ">
                    {{-- <div class="col-xl-2 col-lg-3 col-md-3 col-sm-6 col-6 text-right">
                        <img src="http://demo.sdfcambodia.org/public/frontend/camcyber/mef.png" />
                    </div> --}}

                    {{-- <div class="col-xl-4 col-lg-6 col-md-3 col-sm-6 col-6 text-left">
                        <div class="top-title-cnt mef">
                            <h1>ក្រសួងសេដ្ឋកិច្ច និង ហិរញ្ញវត្ថុ</h1>
                            <h3>Ministry of economic and finance</h3>
                        </div>

                    </div> --}}

                  
                    <div class="col-xl-2 col-lg-3 col-md-3 col-sm-6 col-6 text-right ">
                        <img src="http://sdfcambodia.org/public/frontend/camcyber/sdf.png" />
                    </div>

                    <div class="col-xl-4 col-lg-5 col-md-6 col-sm-6 col-6 text-left">
                        <div class="top-title-cnt">
                            <h1>មូលនិធិអភិវឌ្ឍន៍ជំនាញ</h1>
                            <h3>Skills Development Fund</h3>
                        </div>
                    </div>
                </div>
            </div>

            <div class="container-fluid d-block d-sm-none">
                <div class="row">
                    <div class="col-3 text-center logo-sdf">
                        <img src="http://sdfcambodia.org/public/frontend/camcyber/sdf.png" />
                    </div>
                    <div class="col-9 text-center">
                        <div class="top-title-cnt ">
                            <h1 style="font-size:20px">មូលនិធិអភិវឌ្ឍន៍ជំនាញ </h1>
                            <h3>Skills Development Fund</h3>
                            <!--<h6 style="text-transform: capitalize;">By Ministry of economic and finance</h6>-->
                        </div>
                    </div>

                </div>
            </div>
        </div>

        <div class="bg-light-blue">
            <div class="container">
                <div class="row">
                    <!-- logo -->
                    <div class="col-xl-12 col-sm-12">
                        <div id="navigation">

                            <ul>
                                <li class="@yield('active-home')"><a href=" {{ route('home', $locale) }} " class="animsition-link">{{__('general.home')}}</a></li>

                                <li class="@yield('active-about-us')"><a href=" {{ route('history', $locale) }} " class="animsition-link">{{__('general.about-us')}}

                                </a>
                                    <ul>
                                        <li><a href="{{ route('history' , $locale ) }} " title="" class="animsition-link">{{__('general.history-of-project')}}</a></li>
                                        <li><a href="{{ route('director-speech' , $locale) }}" title="" class="animsition-link">{{__('general.director-speech')}}</a></li>
                                        <li><a href="{{ route('organization' , $locale) }}" title="" class="animsition-link">{{__('general.sdf-organizational-structure')}}</a></li>
                                        {{-- <li><a href="{{ route('organization' , $locale) }}" title="" class="animsition-link">{{__('general.structural-Functions')}}</a></li> --}}
                                    </ul>
                                </li>

                                <li class="@yield('active-scheme')"><a href="{{ route('large-firm', $locale) }}" class="animsition-link">{{__('general.scheme')}}</a>
                                    <ul>
                                        <li><a href=" {{ route('large-firm' , $locale) }} " title="" class="animsition-link">{{__('general.training-large')}}</a></li>
                                        <li><a href="{{ route('sme' , $locale) }}" title="" class="animsition-link">{{__('general.training-for-smes')}}</a></li>
                                        {{-- <li><a href=" {{ route('training-of-trainer' , $locale) }} " title="" class="animsition-link">{{__('general.training-of-trainer')}}</a></li> --}}
                                    </ul>
                                </li>

                                <li class="@yield('active-resources')"><a href="#" class="animsition-link">{{__('general.resources')}}</a>
                                    <ul>
                                        <li><a href=" {{ route('selection', $locale) }} " title="" class="animsition-link">{{__('general.selection-criteria')}}</a></li>
                                        <li><a href=" {{ route('guideline-proposal', $locale) }} " title="" class="animsition-link">{{__('general.guidelines-for-proposal-submission')}}</a></li>
                                        <li><a href="http://www.sdfcambodia.org/public/uploads/file/training-project-proposal-template.doc" title="" class="animsition-link">{{__('general.training-project-proposal')}}</a></li>
                                        <li><a href=" {{ route('faq', $locale) }} " title="" class="animsition-link">{{__('general.faqs')}}</a></li>
                                    </ul>
                                </li>

                                <li class=" @yield('active-news')"><a href="#" class="animsition-link">{{__('general.news-and-events')}}</a>
                                    <ul>
                                        <li><a href="{{ route('news', $locale) }} " class="animsition-link">{{__('general.news')}}</a></li>
                                        <li><a href="{{ route('media', $locale) }} " class="animsition-link">{{__('general.Media')}}</a></li>
                                        <li><a href="{{ route('announcement', $locale) }}" class="animsition-link">{{__('general.announcement')}}</a></li>
                                        <li><a href="{{ route('event', $locale) }}" class="animsition-link">{{__('general.events')}}</a></li>
                                        <li><a href="{{ route('career', $locale) }}" class="animsition-link">{{__('general.careers')}}</a></li>
                                    </ul>
                                </li>

                                <li class="@yield('active-contact-us')"><a href=" {{ route('contact-us' , $locale) }} " class="animsition-link">{{__('general.contact-us')}}</a></li>

                                <li class="language">
                                    <span class="flag-cnt">
                                    
                                        @if($locale == 'en')
                                        
                                            @if($defaultData['routeName'] == 'career-detail')
                                            <a href="{{route('career', $defaultData['khRouteParameters'])}}" @if( $locale=='kh' ) class="active" @endif>​<img src="{{ asset ('public/frontend/images/favicon/kh.png') }}"  style="width="20px;  alt="Responsive image"></a>
                                            @elseif($defaultData['routeName'] == 'news-detail')
                                            <a href="{{route('news', $defaultData['khRouteParameters'])}}" @if( $locale=='kh' ) class="active" @endif>​<img src="{{ asset ('public/frontend/images/favicon/kh.png') }}"  style="width="20px;  alt="Responsive image"></a>
                                            @else
                                            <a href="{{route($defaultData['routeName'], $defaultData['khRouteParameters'])}}" @if( $locale=='kh' ) class="active" @endif>​<img src="{{ asset ('public/frontend/images/favicon/kh.png') }}"  style="width="20px;  alt="Responsive image"></a>
                                            @endif

                                        @else

                                        @if($defaultData['routeName'] == 'career-detail')
                                            <a href="{{route('career', $defaultData['enRouteParameters'])}}" @if( $locale=='en' ) class="active" @endif>​<img src="{{ asset ('public/frontend/images/favicon/en.png') }}"  style="width="20px;  alt="Responsive image"></a>
                                            @elseif($defaultData['routeName'] == 'news-detail')
                                            <a href="{{route('news', $defaultData['enRouteParameters'])}}" @if( $locale=='en' ) class="active" @endif>​<img src="{{ asset ('public/frontend/images/favicon/en.png') }}"  style="width="20px;  alt="Responsive image"></a> 
                                            @else
                                            <a href="{{route($defaultData['routeName'], $defaultData['enRouteParameters'])}}" @if( $locale=='en' ) class="active" @endif>​<img src="{{ asset ('public/frontend/images/favicon/en.png') }}"  style="width="20px;  alt="Responsive image"></a>
                                       
                                            @endif
                                            
                                        @endif
                                    </span>
                                </li>
                                

                            </ul>
                        </div>

                    </div>

                    {{-- <div class="col-xl-1 col-sm-6 d-none d-sm-block">
                        
                    </div> --}}

                </div>
            </div>
        </div>
    </div>

    @yield('content')

    <div class="footer section-space60">
        <!-- footer -->
        <div class="container">
            <div class="row">
                <div class="col-12 text-center">
                    <img style="max-width:150px" src="{{ asset ('public/frontend/camcyber/logo-light.png') }}" alt="">
                    <div class="top-title-cnt">
                        <h1 style="color:white">មូលនិធិអភិវឌ្ឍន៍ជំនាញ</h1>
                        <h3 style="color:white">Skills Development Fund</h3>
                    </div>
                </div>

            </div>
            <hr class="dark-line">
            <div class="row">
                <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                    <div class="widget-text mt40">
                        <!-- widget text -->
                        {{-- <p style="text-indent: 0px;">{{__('general.description-address')}}</p> --}}
                        <div class="row">
                            <div class="col-12">
                                    <div class="absolutetop">
                                        <i class="fa fa-map-marker" style="font-size: 20px;width: 25px;color:white;"></i>
                                    </div>
                                    <div style="margin-left:30px; color:white;">
                                        {{__('general.email-address-contact')}}
                                    </div>
                                {{-- <p style="text-indent: 0px;" class="address-text">
                                    <span><i class="icon-placeholder-3 icon-1x"></i> </span> 
                                    {{__('general.address')}}
                                </p> --}}
                            </div>
            
                            <div class="col-12 mt-2">
                                <div class="absolutetop">
                                   <i class="fa fa-phone" style="font-size: 20px;width: 25px;color:white;"></i>
                                </div>
                                <div style="margin-left:30px;color:white;">
                                    {{__('general.phone-number2')}}
                                </div>
                            </div>

                        </div>
                    </div>
                    <!-- /.widget text -->
                </div>

                <div class="col-xl-2 col-lg-2 col-md-4 col-sm-6 col-6">
                    <div class="widget-footer mt40">
                        <!-- widget footer -->
                        <ul class="listnone">
                            <li><a href=" {{ route('home', $locale) }} " class="animsition-link">{{__('general.home')}}</a></li>
                            <li><a href=" {{ route('history', $locale) }} " class="animsition-link">{{__('general.about-us')}}</a></li>
                            <li><a href="{{ route('large-firm', $locale) }}" class="animsition-link">{{__('general.scheme')}}</a></li>
                           
                        </ul>
                    </div>
                    <!-- /.widget footer -->
                </div>

                <div class="col-xl-2 col-lg-2 col-md-4 col-sm-6 col-6">
                    <div class="widget-footer mt40">
                        <!-- widget footer -->
                        <ul class="listnone">
                            <li><a href=" {{ route('selection', $locale) }} " title="" class="animsition-link">{{__('general.resources')}}</a></li>
                            <li><a href=" {{ route('news', $locale) }} " class="animsition-link">{{__('general.news')}}</a></li>
                            <li><a href=" {{ route('contact-us' , $locale) }} " class="animsition-link">{{__('general.contact-us')}}</a></li>
                        </ul>
                    </div>
                    <!-- /.widget footer -->
                </div>

                <div class="col-xl-2 col-lg-6 col-md-4 col-sm-12 col-12">
                    <div class="widget-social mt40">
                        <!-- widget footer -->
                        <ul class="listnone">
                            <li><a target="_blank" href="https://www.facebook.com/sdfcambodia/"><i class="fa fa-facebook"></i>Facebook</a></li>
                            <li><a  target="_blank" href="https://www.linkedin.com/company/skills-development-fund-cambodia/?viewAsMember=true"><i class="fa fa-linkedin"></i>Linked In</a></li>
                            <li><a target="_blank" href="https://www.youtube.com/channel/UC77uLx8E03HH9KwQ8QRbZQg/featured?view_as=subscriber"><i class="fa fa-youtube"></i>YouTube</a></li>
                        </ul>
                    </div>
                    <!-- /.widget footer -->
                </div>
            </div>
        </div>
    </div>

    <!-- /.footer -->
    <div class="tiny-footer">
        <!-- tiny footer -->
        <div class="container">
            <div class="row">
                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-6">
                <p style="text-indent:0px;">{{__('general.copyright')}}</p>
                </div>
                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-6 text-right">
                    <p>{{__('general.ministry-of-economic-and-finance')}}</p>

                </div>
            </div>
        </div>
    </div>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="{{ asset ('public/frontend/js/jquery.min.js') }}"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="{{ asset ('public/frontend/js/bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset ('public/frontend/js/menumaker.js') }}"></script>

    <!-- sticky header -->
    <script type="text/javascript" src="{{ asset ('public/frontend/js/jquery.sticky.js') }}"></script>
    <script type="text/javascript" src="{{ asset ('public/frontend/js/sticky-header.js') }}"></script>
    <!-- slider script -->
    <script type="text/javascript" src="{{ asset ('public/frontend/js/owl.carousel.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset ('public/frontend/js/slider-carousel.js') }}"></script>
    <script type="text/javascript" src="{{ asset ('public/frontend/js/service-carousel.js') }}"></script>

     <!-- FAQ -->
    <script type="text/javascript" src="{{ asset ('public/frontend/js/accordion.js') }}"></script>
    @yield('appbottomjs')
   <script type="text/javascript">
       $('.partner-owl').owlCarousel({
            loop:true,
            margin:0,
            nav:false,
            responsive:{
                0:{
                    items:1
                },
                600:{
                    items:3
                },
                1000:{
                    items:5
                }
            }
        })
   </script>



</body>

</html>

<style>
.language .flag-cnt img{
    margin-top:20px;
    float:right;
}
@media (min-width:768px){
    .language{
        float:right !important;
        margin-bottom: 20px;
    }
}
@media (max-width:768px){
    .language{
        float:left !important;
    }
    .language .flag-cnt img{
        margin-top:20px;
        margin-bottom:20px;
        float:left;
        margin-left:25px;
    }
}
.absolutetop {
    position: absolute;
    z-index: 99;
}

</style>