<?php

namespace App\Http\Controllers\CP\Resource;

use Auth;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;
use Session;
use Illuminate\Validation\Rule;
use App\Http\Controllers\CamCyber\FileUploadController as FileUpload;
use App\Http\Controllers\CamCyber\FunctionController;


use App\Model\Content;


class ResourceController extends Controller
{
    
    function __construct (){
       $this->route = "cp.resource.selection-criteria";
    }

    public function update(Request $request, $slug=""){   
      // dd($request);
     
     
      $data = array( 
                   
                    'en_content' =>   $request->input('en_content'),
                    'kh_content' =>   $request->input('kh_content'),
                );

     
      // Validator::make($request->all(), $validate)->validate();
  
   
      Content::where('slug', $slug)->update($data);

      Session::flash('msg', 'Data has been updated!' );
      return redirect()->back(); 
    }

  

    public function view($slug=''){ 
      $data = Content::select('*')
      ->where('slug', $slug)
      ->first();

    
     

      if($data){
        return view('cp.resource.edit', ['route'=>$this->route,'data'=>$data]);
      }else{
        return response(view('errors.404'), 404);
      }
    }
   
}
