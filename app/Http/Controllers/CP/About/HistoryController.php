<?php

namespace App\Http\Controllers\CP\About;

use Auth;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;
use Session;
use Illuminate\Validation\Rule;
use App\Http\Controllers\CamCyber\FileUploadController as FileUpload;
use App\Http\Controllers\CamCyber\FunctionController;


use App\Model\Content;


class HistoryController extends Controller
{
    
    function __construct (){
       $this->route = "cp.about.history";
    }

    public function update(Request $request){   
      $id = $request->input('id');
      $image = "";
     
      $data = array( 
                   
                    // 'en_content' =>   $request->input('en_content'),
                    // 'kh_content' =>   $request->input('kh_content'),
                    'kh_content_part1' =>   $request->input('kh_content_part1'),
                    'en_content_part1' =>   $request->input('en_content_part1'),
                    'kh_content_part2' =>   $request->input('kh_content_part2'),
                    'en_content_part2' =>   $request->input('en_content_part2'),
                );

      
      // Validator::make($request->all(), $validate)->validate();
  
   
      Content::where('slug', 'history')->update($data);

      Session::flash('msg', 'Data has been updated!' );
      return redirect()->back(); 
    }

  

    public function view(){ 

      $data = Content::select('*')
      ->where('slug', 'history')
      ->first();

      if(!empty($data)){
        return view('cp.about.history.edit', ['route'=>$this->route,'data'=>$data]);
      }else{
        return response(view('errors.404'), 404);
      }
    }
   
}
