<?php

namespace App\Http\Controllers\Frontend;
use App\Http\Controllers\Frontend\FrontendController;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use App\Model\Slide as Slide;
use App\Model\Image as Image;
use App\Model\PrioritySector as PrioritySector;
use App\Model\Partner as Partner;
use App\Model\Application as Application;
use App\Model\News as News;


class HomeController extends FrontendController
{

    public function index($locale="kh"){
		
    	$defaultData = $this->defaultData($locale);
    	$slide = Slide::select('id', $locale.'_title as title','image')->where('is_published', 1)->get();
    	$image = Image::select('id','title','image','content')->get();
    	$priority_sector = PrioritySector::select('id',$locale.'_title as title','image','content')->get();
    	$partner = Partner::select('id','title','url','image','content')->get();
    	$application = Application::select('id',$locale.'_title as title','image', $locale.'_content as content')->get();
		
		$relatedPosts = News::select('id', $locale.'_title as title', $locale.'_content as content', $locale.'_description as description','is_published','is_featured','image','date')
        ->orderBy('date','DESC')
        ->where(['is_published'=>1])
        ->limit(3)->get();
		
    	return view('frontend.home',['locale'=>$locale,'slide' => $slide,'defaultData' => $defaultData,'image'=>$image,'priority_sector'=> $priority_sector,'partner'=>$partner,'application'=>$application,'relatedPosts'=>$relatedPosts]);
	}
	
	public function getApplication(Request $request, $locale="kh"){
		$defaultData = $this->defaultData($locale);
		$data = Application::select('id',$locale.'_title as title','image', $locale.'_content as content')
				->where('id', $request->id)->first();

        return response()->json(array('success' => true, 'data' => $data));
	}
}
