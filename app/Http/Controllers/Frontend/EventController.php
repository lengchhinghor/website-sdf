<?php

namespace App\Http\Controllers\Frontend;
use App\Http\Controllers\Frontend\FrontendController;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Model\Event;
use App\Model\Banner;
class EventController extends FrontendController
{

    public function listing($locale="en"){
        $defaultData = $this->defaultData($locale);
        $banner = Banner::select('id','is_published','image')->find(14);
        $data = Event::select('id',$locale.'_title as title',
        $locale.'_location as location',
        $locale.'_description as description',
        'date','time','image','is_published')
        ->where(['is_published'=>1])
        ->get();
         
        return view('frontend.event.listing',
            ['locale'=>$locale,
            'defaultData' => $defaultData,'data' => $data,'banner'=>$banner]);
    }
}
