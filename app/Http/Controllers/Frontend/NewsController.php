<?php

namespace App\Http\Controllers\Frontend;
use App\Http\Controllers\Frontend\FrontendController;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Model\News;
use App\Model\Banner;
class NewsController extends FrontendController
{

    public function listing($locale="en"){
        $defaultData = $this->defaultData($locale);
        $banner = Banner::select('id','is_published','image')->find(14);
        // dd($banner);
        $data   = News::select(
            'id',
            $locale.'_title as title',
            $locale.'_content as content',
            $locale.'_description as description',
            'is_published','is_featured',
            'image',
            'date'
        )->where('is_published',1)->orderBy('date','DESC')->paginate(5);

    	return view('frontend.news.listing',['locale'=>$locale,'defaultData' => $defaultData,'data'=>$data,'banner'=>$banner]);
    }

    public function read( $locale="en", $id=0){

        $defaultData = $this->defaultData($locale);
        $banner = Banner::select('id','is_published','image')->find(14);
	    $data = News::select('id', $locale.'_title as title',$locale.'_content as content',$locale.'_description as description','is_published','is_featured','image','date')->where(['id'=>$id])->first(); 
        $relatedPosts =News::select('id', $locale.'_title as title', $locale.'_content as content', $locale.'_description as description','is_published','is_featured','image')
        ->where(['is_published'=>1])
        ->where('id', '<>', $id)
        ->orderBy('id','DESC')
        ->limit(4)
        ->get();
	    
        return view('frontend.news.detail',[
            'locale'=>$locale,
            'defaultData' => $defaultData,
            'data'=>$data,
            'relatedPosts'=>$relatedPosts,
            'banner' => $banner,
        ]); 
    }

}
