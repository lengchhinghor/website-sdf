<?php

namespace App\Http\Controllers\Frontend;
use App\Http\Controllers\Frontend\FrontendController;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Model\Announcement;
use App\Model\Banner;
class AnnouncementController extends FrontendController
{

    public function listing($locale="en"){
        $defaultData = $this->defaultData($locale);
        $banner = Banner::select('id','is_published','image')->find(14);
        $data = Announcement::select(
            'id',
            $locale.'_title as title',
            'is_published','is_featured',
            'created_at','file')
            ->paginate(5);

        return view('frontend.announcement.listing',
        ['locale'=>$locale,
        'defaultData' => $defaultData,
        'data'=>$data,
        'banner'=>$banner]);
    }
}
