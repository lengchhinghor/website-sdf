<?php

namespace App\Http\Controllers\Frontend;
use App\Http\Controllers\Frontend\FrontendController;
use App\Http\Controllers\CamCyber\FileUploadController as FileUpload;
use Illuminate\Http\Request;
use App\Http\Requests;
use Session;
use Validator;
use App\Model\Career;
use App\Model\Banner;
use App\Model\Message as Message;
class CareerController extends FrontendController
{

    public function listing($locale="en"){
        $defaultData = $this->defaultData($locale);
        $banner = Banner::select('id','is_published','image')->find(14);
        $data = Career::select(
            'id',
            $locale.'_title as title',
            $locale.'_content as content',
            $locale.'_description as description',
            'is_published','is_featured',
            'dateline'
        )->paginate(5);

        return view('frontend.career.listing',['locale'=>$locale,'defaultData' => $defaultData,'data'=>$data,'banner'=>$banner]);
    }

    public function read( $locale="en", $id=0){

        $defaultData = $this->defaultData($locale);
        $banner = Banner::select('id','is_published','image')->find(14);
        $data = Career::select(
            'id', 
            $locale.'_title as title',
            $locale.'_content as content',
            $locale.'_description as description',
            'is_published','is_featured')
        ->where(['id'=>$id])
        ->first(); 

             $relatedPosts =Career::select('id', $locale.'_title as title', 
             $locale.'_content as content',
             $locale.'_description as description',
             'is_published','is_featured','image')
        ->where(['is_published'=>1])
        ->where('id', '<>', $id)
        ->orderBy('id','DESC')
        ->limit(4)
        ->get();
        
        return view('frontend.career.detail',[
            'locale'=>$locale,
            'defaultData' => $defaultData,
            'data'=>$data,
            'relatedPosts'=>$relatedPosts,
            'banner' => $banner,
        ]); 
    }

     public function sendMessage(Request $request){
      
        $now        = date('Y-m-d H:i:s');
        
        $data = array(
            'career_id'     => $request->input('career_id'),
            'name'           =>      $request->input('name'),
            'phone'          =>      $request->input('phone'),
            'email'          =>      $request->input('email'),
            
            'created_at'     =>      $now
        );
        $file = FileUpload::uploadFile($request, 'file', 'uploads/file');
        if($file != ""){
            $data['file'] = $file; 
        }
        Session::flash('invalidData', $data );
        Validator::make(
            $request->all(), 
            [
            
                'name' => 'required',
                'phone' => 'required',
                'email' => 'required',
            ], 
            [
                
            ])->validate();

        
        $id = Message::insertGetId($data);
        
        $request->session()->flash('msg', 'Your request has been sent! We will respone you soon.' );
        return redirect()->back();
    }

}
