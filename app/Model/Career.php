<?php

namespace App\Model;
use Illuminate\Database\Eloquent\Model;

class Career extends Model
{
   
    protected $table = 'career';
   
    public function message() {
        return $this->hasMany('App\Model\Message');
    }
   
}
