<?php

namespace App\Model;
use Illuminate\Database\Eloquent\Model;

class Faq extends Model
{
   
    protected $table = 'faq';

   	public function category(){
        return $this->belongsTo('App\Model\Category', 'category_id');
    }
   
}
